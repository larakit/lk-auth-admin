<?php
/**
 * Created by Larakit.
 * Link: http://github.com/larakit
 * User: Alexey Berdnikov
 * Date: 10.08.17
 * Time: 17:26
 */

namespace Larakit\Auth;

class UserObserverAdmin {
    
    public function saving($model) {
        $model->is_admin = (int) $model->is_admin;
    }
}