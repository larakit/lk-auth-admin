<?php
\Larakit\Boot::register_middleware_route('admin', \Larakit\Auth\MiddlewareAdmin::class);
\Larakit\Boot::register_migrations(__DIR__ . '/migrations');

\Larakit\Boot::register_observer_user(\Larakit\Auth\UserObserverAdmin::class);