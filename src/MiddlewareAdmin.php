<?php

namespace Larakit\Auth;

use Closure;
use Larakit\Event\Event;

class MiddlewareAdmin {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!(bool) env('skip_auth')) {
            if(!me('is_admin')) {
                Event::notify('larakit::no_admin');
                return \Redirect::to('/?no_admin');
            }
        }
        
        return $next($request);
    }
}